# Slim jQuery modal plugin

## Code Example

### HTML

```html
<head>
    ...
    <link rel="stylesheet" href="jquery-slimModal.css" type="text/css" media="all" />
</head>
<body>
    <div id="modal">
        Loading Google...
    </div>

    ...

    <script type="text/javascript" src="jquery-slimModal.min.js"></script>
</body>

```

### JAVASCRIPT
```javascript
$("#modal").slimModal().show().load("http://google.com");
```

## License

Under Creative Commons License

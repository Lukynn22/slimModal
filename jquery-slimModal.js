/**
 * Created by Lukáš Materna
 */

;(function($){
    $.fn.extend({
        slimModal: function(options) {
            this.defaultOptions = {
                width: 650,
                height: 465,
                cloneAllowed: false,
                showOverlay: true,
                targetElem: "body",
                useEscape: true
            };

            var settings = $.extend({}, this.defaultOptions, options);

            this.close = function (event) {
                close($(this));
            };

            function close($el) {
                if ($el.hasClass("noclone")) {
                    $el.hide();

                } else {
                    $el.remove();
                }
                $(".modalWindow_overlay").remove();
            }

            this.show = function (event) {
                var $modal_clone;

                if (settings.cloneAllowed) {
                    $modal_clone = $(this).clone(true, true);

                } else {
                    $modal_clone = $(this).addClass("noclone");
                }

                if (settings.showOverlay) {
                    var modalWindow_overlay = $("<div class='modalWindow_overlay'>").css({
                        'width': $(window).width() + 'px',
                        'height': $(window).height() + 'px',
                    });
                    $(settings.targetElem).append(modalWindow_overlay.show());
                }

                $(settings.targetElem).append($modal_clone);
                $modal_clone.addClass('modalWindow').css({
                    'top': ($(window).height() / 2) - (settings.height / 2) + 'px',
                    'left': 'calc(50% - ' + (settings.width / 2) + 'px)',
                    'width': settings.width,
                    'max-height': settings.height
                }).show().append("<div class='close_modal'>");

                //Ways to close modal window
                $(".close_modal").on("click", function () {
                    close($modal_clone);
                });
                if (settings.showOverlay) {
                    modalWindow_overlay.on("click", function () {
                        close($modal_clone);
                    });
                }
                if (settings.useEscape) {
                    $(document).keyup(function (e) {
                        if (e.keyCode == 27) {
                            close($modal_clone);
                        }
                    });
                }

                return $modal_clone;
            };

            return this.each(function() {
                var $this = $(this);
                $this.hide();
            });
        }
    });
})(jQuery);
